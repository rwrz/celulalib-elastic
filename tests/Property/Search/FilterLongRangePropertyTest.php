<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 09/05/17
 * Time: 16:07
 */

namespace CelulaLibTest\Elastic\Property\Search;

use CelulaLib\Elastic\Property\Search\FilterLongRangeProperty;
use CelulaLib\Elastic\Property\Search\SearchParam;
use PHPUnit\Framework\TestCase;

class FilterLongRangePropertyTest extends TestCase
{
    public function testInvalidConstructor()
    {
        // Exception sem Properties
        $this->expectException(\InvalidArgumentException::class);
        $simpleProperty = new FilterLongRangeProperty('', '');
    }

    public function testValidConstructor()
    {
        $name = 'teste';
        $parameterName = 'teste_par';

        $simpleProperty = new FilterLongRangeProperty($name, $parameterName);

        $this->assertEquals($name, $simpleProperty->getName());
        $this->assertEquals($parameterName, $simpleProperty->getParameterName());
    }

    public function testEmptyParameterName()
    {
        $simpleProperty = new FilterLongRangeProperty("teste");

        $this->assertEquals("teste", $simpleProperty->getParameterName());

        $simpleProperty = new FilterLongRangeProperty("teste", "teste2");

        $this->assertEquals("teste2", $simpleProperty->getParameterName());
    }

    public function testFilter()
    {
        $data = new \DateTime();
        $params = array (
            'inicio' => 1,
            'fim' => 10
        );
        $dateProperty = new FilterLongRangeProperty('teste');
        $result = $dateProperty->getFilter(new SearchParam($params), '');
        $expected = array (
            'range' => array (
                'teste' => array(
                    'gte'    => 1,
                    'lte'    => 10,
                    'relation' => 'intersects'
                )
            )
        );

        $this->assertEquals($expected, $result);
    }

    public function testFilterArrayOneValue()
    {
        $params = array (
            'inicio' => 1
        );
        $dateProperty = new FilterLongRangeProperty('teste');
        $result = $dateProperty->getFilter(new SearchParam($params), '');
        $expected = array (
            'range' => array (
                'teste' => array(
                    'gte'    => 1,
                    'lte'    => 1,
                    'relation' => 'intersects'
                )
            )
        );

        $this->assertEquals($expected, $result);
    }
}
