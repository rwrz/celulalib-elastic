<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 04/04/17
 * Time: 14:56
 */

namespace CelulaLib\Elastic\Property;

interface PropertyCollectionInterface
{
    public function getName() : string;
    public function getData() : array;
    /**
     * @return PropertyInterface[]
     */
    public function getProperties();
}