<?php

namespace CelulaLib\Elastic;

use Application\Service\FileServer;
use CelulaLib\CelulaNFE;
use CelulaLib\CelulaSimpleXMLElement;
use CelulaLib\CelulaTika\CelulaTika;
use Elasticsearch\Client;
use GEDDigital\Entity\Documento as DocumentoEntity;
use GEDDigital\Enum\ElasticIndexing;
use GEDDigital\Model\ArquivoDigital;
use GEDDigital\Model\Documento as DocumentoModel;

class Updater extends AbstractCelulaElastic
{
    /**
     * @var FileServer
     */
    private $fileServer;

    /**
     * @var DocumentoModel
     */
    private $documentoModel;

    /**
     * @var ArquivoDigital
     */
    private $arquivoDigitalModel;

    public function __construct(Client $eClient, $documentos, FileServer $fileServer, DocumentoModel $documentoModel, ArquivoDigital $arquivoDigitalModel)
    {
        $this->eClient = $eClient;
        $this->documentos = $documentos;
        $this->fileServer = $fileServer;
        $this->documentoModel = $documentoModel;
        $this->arquivoDigitalModel = $arquivoDigitalModel;
    }

    public function execute(string $option = ElasticIndexing::ALL)
    {
        $codigosDocumentosIndexados = array();
        $codigosArquivosIndexados = array();
        $codigosArquivosProcessados = array();

        /** @var DocumentoEntity $documento */
        foreach ($this->documentos as $documento) {
            $this->params = array(
                'index' => 'ged',
                'type' => 'documento',
                'id' => $documento->getCodigo(),
                'body' => array(
                    'doc' => array(
                        'arquivos_indexados' => true,
                        'arquivos' => array()
                    )
                )
            );
            switch ($option) {
                case ElasticIndexing::CRITERIOS:
                    $this->processCriterios($documento);
                    break;
                case ElasticIndexing::ARQUIVOS:
                    $this->processArquivos($documento, $codigosArquivosIndexados, $codigosArquivosProcessados);
                    $codigosDocumentosIndexados[] = $documento->getCodigo();
                    break;
                case ElasticIndexing::ALL:
                    $this->processCriterios($documento);
                    $this->processArquivos($documento, $codigosArquivosIndexados, $codigosArquivosProcessados);
                    $codigosDocumentosIndexados[] = $documento->getCodigo();
                    break;
            }
            parent::execute();
        }
        if (!empty($codigosArquivosIndexados)) {
            $this->arquivoDigitalModel->atualizarStatusIndexado($codigosArquivosIndexados);
        }
        if (!empty($codigosArquivosProcessados)) {
            $this->arquivoDigitalModel->atualizarStatusProcessado($codigosArquivosProcessados);
        }
        if (!empty($codigosDocumentosIndexados)) {
            $this->documentoModel->atualizarStatusIndexado($codigosDocumentosIndexados);
        }
    }

    /**
     * Processa os Arquivos Digitais
     * @param DocumentoEntity $documento
     * @param $codigosArquivosIndexados
     * @param $codigosArquivosProcessados
     * @throws \Exception
     */
    private function processArquivos(DocumentoEntity $documento, &$codigosArquivosIndexados, &$codigosArquivosProcessados)
    {
        foreach ($documento->getArquivosDigitais() as $arquivoDigital) {
            $codigosArquivosIndexados[] = $arquivoDigital->getCodigo();

            $properties = array (
                'codigo' => (int) $arquivoDigital->getCodigo(),
                'nome' => $arquivoDigital->getNome(),
                'data_inclusao' => $arquivoDigital->getDataInclusao(),
                'tamanho' => $arquivoDigital->getTamanho(),
                'nfe' => false,
                'extensao' => array(
                    'codigo' => (int) $arquivoDigital->getExtensao()->getCodigo(),
                    'nome' => $arquivoDigital->getExtensao()->getNome(),
                    'icone' => $arquivoDigital->getExtensao()->getIcone()
                )
            );

            if ($arquivoDigital->isProcessado() !== null) {
                $path = realpath('./public/tmp') . '/processable' . $arquivoDigital->getS3Name();
                $this->fileServer->download(
                    $documento->getUsuario(),
                    $arquivoDigital,
                    $path
                );
                $properties['conteudo'] = CelulaTika::getText($path);
                $codigosArquivosProcessados[] = $arquivoDigital->getCodigo();

                if($arquivoDigital->getExtensao()->isXml()) {
                    $xml = CelulaSimpleXMLElement::createFromPath($path);
                    if($xml->isNFE()) {
                        $properties['nfe'] = true;
                        $this->arquivoDigitalModel->setAsNFE($arquivoDigital);
                        $celulaNFE = new CelulaNFE($xml);
                        $this->eClient->update($celulaNFE->getContentForElastic($arquivoDigital));
                    }
                }
                unlink($path);
            }

            if ($arquivoDigital->getExtensao()->isDwg()){

                $caminhoS3fileDwg = $this->fileServer->getS3Path($documento->getUsuario(),$arquivoDigital);
                $caminhoS3fileSvg = str_replace('.dwg', '.svg', $caminhoS3fileDwg);

                $configS3 = $this->fileServer->getConfig();

                $client = new \Zend\Soap\Client("https://webservice.celula.net.br/GEDConvert/GEDConvert.svc?wsdl");
                $client->setSoapVersion(SOAP_1_1);

                $configRequisicao =  array(
                    'caminhoS3fileDwg' => $caminhoS3fileDwg,
                    'caminhoS3fileSvg' => $caminhoS3fileSvg,
                    'bucketName'       => $configS3['bucket']
                );

                $result = $client->S3DwgToSvg($configRequisicao);

                if(!($result->S3DwgToSvgResult === 'success')){
                    throw new \Exception($result->S3DwgToSvgResult);
                }

                $codigosArquivosProcessados[] = $arquivoDigital->getCodigo();
            }

            if ($arquivoDigital->isPrincipal()) {
                $this->params['body']['doc']['arquivo_principal'] = $properties;
            } else {
                $this->params['body']['doc']['arquivos'][] = $properties;
            }
        }
    }
}