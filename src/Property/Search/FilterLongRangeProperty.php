<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 09/05/17
 * Time: 15:07
 */

namespace CelulaLib\Elastic\Property\Search;

use CelulaLib\Elastic\Property\Create\LongRangeProperty;

class FilterLongRangeProperty extends LongRangeProperty implements FilterInterface
{
    use TraitSearchableProperty; // Implements getParameterName, setParameterName from Searchable

    /**
     * FilterLongRangeProperty constructor.
     * @param string $name
     * @param string $parameterName
     */
    public function __construct(string $name, $parameterName = '')
    {
        parent::__construct($name);
        $this->setParameterName($parameterName);
    }

    /**
     * @param SearchParam[]|SearchParam $param
     * @param string $parentName
     * @return array
     */
    public function getFilter($param, string $parentName): array
    {
        $valor = $param->getValor();

        $begin = $valor;
        $end = $valor;
        if (is_array($valor)) {
            $tmp = array_values($valor);
            $begin = $tmp[0];

            if ((count($tmp) == 2) && !empty($tmp[1])) {
                $end = $tmp[1];
            } else {
                $end = $tmp[0];
            }
        }

        return array(
            'range' => array(
                $this->getFullName($parentName) => array(
                    'gte'    => $begin,
                    'lte'    => $end,
                    'relation' => 'intersects'
                )
            )
        );
    }
}