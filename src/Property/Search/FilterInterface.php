<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 06/04/17
 * Time: 16:00
 */

namespace CelulaLib\Elastic\Property\Search;

interface FilterInterface extends SearchablePropertyInterface
{
    /**
     * @param SearchParam[]|SearchParam $param
     * @param string $parentName
     * @return array
     */
    public function getFilter($param, string $parentName): array;
}
