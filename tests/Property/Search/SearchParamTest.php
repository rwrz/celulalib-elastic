<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 09/05/17
 * Time: 16:03
 */

namespace CelulaLibTest\Elastic\Property\Create;

use CelulaLib\Elastic\Property\Search\SearchParam;
use CelulaLib\Elastic\Property\Search\SearchType;
use PHPUnit\Framework\TestCase;

class SearchParamTest extends TestCase
{
    public function testInvalidConstructor()
    {
        // Exception sem Properties
        $this->expectException(\InvalidArgumentException::class);
        $simpleProperty = new SearchParam('');
    }

    public function testValidConstructor()
    {
        $valor = 'rodrigo';
        $type = SearchType::IGUAL_A;

        $simpleProperty = new SearchParam($valor, $type);

        $this->assertEquals($valor, $simpleProperty->getValor());
        $this->assertEquals($type, $simpleProperty->getSearchType());
    }

    public function testMethods()
    {
        $valor = 'rodrigo';
        $type = SearchType::IGUAL_A;

        $simpleProperty = new SearchParam($valor, $type);

        $this->assertEquals($valor, $simpleProperty->getValor());
        $this->assertEquals($type, $simpleProperty->getSearchType());

        $simpleProperty->setValor('aloha');
        $simpleProperty->setSearchType(SearchType::MAIOR_QUE);

        $this->assertEquals('aloha', $simpleProperty->getValor());
        $this->assertEquals(SearchType::MAIOR_QUE, $simpleProperty->getSearchType());
    }
}
