<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 30/03/17
 * Time: 10:39
 */

namespace CelulaLib\Elastic;

use CelulaLib\Elastic\Property\PropertyCollectionInterface;
use CelulaLib\Elastic\Property\PropertyInterface;

class Mapping implements PropertyCollectionInterface
{
    /**
     * Atributos do Mapping
     * @var array
     */
    protected $attributes = array();

    protected $defaultAttributes = array (
        "dynamic_date_formats" => "yyyy-MM-dd HH:mm:ss",
        "_source" => array(
            "enabled" => true
        ),
    );

    /**
     * Inject default attributes or not
     * @var bool
     */
    protected $shouldInjectDefaultAttributes = true;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var PropertyInterface[]
     */
    protected $properties;

    /**
     * Mapping constructor.
     * @param string $name
     * @param array $properties
     * @param array $attributes
     * @throws \Exception
     */
    public function __construct(string $name, array $properties, array $attributes)
    {
        if (empty($name) || empty($properties)) {
            throw new \InvalidArgumentException("Mapping precisa de um nome e propriedades");
        }

        $this->name = $name;
        $this->properties = $properties;

        $this->setAttributes($attributes);
    }

    public function getData(): array
    {
        // Pega atributos
        $attributes = array();
        if ($this->shouldInjectDefaultAttributes) {
            $attributes = $this->getInjectedDefaultAttributes();
        } else {
            $attributes = $this->getAttributes();
        }

        $properties = array ();
        foreach ($this->properties as $property) {
            $properties = array_merge_recursive($properties, $property->getData());
        }

        return $attributes + array('properties' => $properties);
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @param bool $shouldInjectDefaultAttributes
     */
    public function setShouldInjectDefaultAttributes(bool $shouldInjectDefaultAttributes)
    {
        $this->shouldInjectDefaultAttributes = $shouldInjectDefaultAttributes;
    }

    protected function getInjectedDefaultAttributes()
    {
        return $this->getAttributes() + $this->defaultAttributes;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return PropertyInterface[]
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * @param PropertyInterface[] $properties
     */
    public function setProperties(array $properties)
    {
        $this->properties = $properties;
    }

    public function getValue($value): array
    {
        // TODO: Implement getValue() method.
        return array();
    }

    /**
     * Method should be overriden if your mapping accepts something different of array data
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function convertDataToInsert(&$data) : array
    {
        if (!is_array($data)) {
            throw new \Exception("para inserir nos mappings é necessário passar um array, caso contrário, 
                implemente o metodo convertDataToInsert convertendo seus dados para array");
        }

        return $data;
    }
}