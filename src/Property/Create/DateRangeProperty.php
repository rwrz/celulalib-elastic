<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 09/05/17
 * Time: 15:08
 */

namespace CelulaLib\Elastic\Property\Create;

class DateRangeProperty extends DateProperty implements FormattedValueInterface
{
    /**
     * DateRangeProperty constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->type = 'date_range';
    }

    /**
     * Formata o valor na hora do insert/update
     * @param $value mixed
     * @return mixed|string
     */
    public function formatValue($value)
    {
        if (!is_array($value) || (count($value) < 2)) {
            throw new \InvalidArgumentException('Date Range exige um array para inserir, 
                e o mesmo deve possuir 2 elementos');
        }

        // corrige o array, eliminando o ASSOC
        $value = array_values($value);

        // Corrige as datas para datetime caso necessário
        $value[0] = $this->fixDate($value[0]);
        $value[1] = $this->fixDate($value[1]);

        // Datas invertidas? sem problemas!
        if ($value[0] > $value[1]) {
            $aux = $value[0];
            $value[0] = $value[1];
            $value[1] = $aux;
        }

        $return = [];
        if ($value[0] instanceof \DateTime) {
            $value[0] = $value[0]->format('Y-m-d H:i:s');
        }
        if ($value[1] instanceof \DateTime) {
            $value[1] = $value[1]->format('Y-m-d H:i:s');
        }
        $return['gte'] = $value[0];
        $return['lte'] = $value[1];

        return $return;
    }
}