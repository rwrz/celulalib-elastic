<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 03/04/17
 * Time: 16:55
 */
namespace CelulaLib\Elastic\Property\Create;

use CelulaLib\Elastic\Property\PropertyInterface;

abstract class AbstractProperty implements PropertyInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * AbstractProperty constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        if (empty($name)) {
            throw new \InvalidArgumentException('nome é obrigatorio');
        }

        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue($value): array
    {
        $aux = array();
        if ($this instanceof FormattedValueInterface) {
            $aux[$this->getName()] = $this->formatValue($value);
        } else {
            $aux[$this->getName()] = $value;
        }

        return $aux;
    }

    public function getHighlight(string $parentName): array
    {
        return array (
            array (
                $this->getFullName($parentName) => new \stdClass()
            )
        );
    }

    public function getFullName(string $parentName)
    {
        return (!empty($parentName) ? $parentName . '.' : '') . $this->getName();
    }
}