<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 11/05/17
 * Time: 14:43
 */

namespace CelulaLib\Elastic\Property\Create;

class DynamicProperty extends AbstractProperty
{
    const FROM_MUST = 'must';
    const FROM_FILTER = 'filter';
    const FROM_SHOULD = 'should';

    /**
     * @var SimpleProperty[]
     */
    protected $properties = array();

    /**
     * Add properties to the dynamic property
     * @param SimpleProperty $property
     */
    public function addProperty(SimpleProperty $property)
    {
        if (!array_key_exists($property->getType(), $this->properties)) {
            $this->properties[$property->getType()] = $property;
        } else {
            throw new \InvalidArgumentException("Tipo de propriedade já existente na propriedade dinamica");
        }
    }

    /**
     * @param $type
     * @return bool
     */
    public function removeProperty($type)
    {
        if (array_key_exists($type, $this->properties)) {
            unset($this->properties[$type]);
        } else {
            return false;
        }

        return true;
    }

    /**
     * Return CREATION array
     * @return array
     */
    public function getData(): array
    {
        $aux = array();
        foreach ($this->properties as $property) {
            $aux = array_merge_recursive($aux, $property->getData());
        }

        return $aux;
    }

    /**
     * Return INSERTION array
     * @param $value
     * @return array
     */
    public function getValue($value): array
    {
        $aux = array();
        foreach ($this->properties as $property) {
            $aux = array_merge_recursive($aux, $property->getValue($value));
        }
        return $aux;
    }

    /**
     * Return HIGHLIGHT array
     * @param string $parentName
     * @return array
     */
    public function getHighlight(string $parentName): array
    {
        $aux = array();
        foreach ($this->properties as $property) {
            $aux = array_merge_recursive($aux, $property->getHighlight($parentName));
        }
        return $aux;
    }
}