<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 07/04/17
 * Time: 16:22
 */

namespace CelulaLib\Elastic\Property\Search;

trait TraitSearchableProperty
{
    /**
     * @var string
     */
    private $parameterName;

    public function getParameterName(): string
    {
        return $this->parameterName;
    }

    public function setParameterName($parameterName)
    {
        if (empty($parameterName)) {
            $this->parameterName = $this->getName();
        } else {
            $this->parameterName = $parameterName;
        }
    }
}
