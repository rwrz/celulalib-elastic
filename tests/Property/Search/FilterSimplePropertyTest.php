<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 07/04/17
 * Time: 16:28
 */

namespace CelulaLibTest\Elastic\Property\Search;

use CelulaLib\Elastic\Property\PropertyType;
use CelulaLib\Elastic\Property\Search\FilterSimpleProperty;
use CelulaLib\Elastic\Property\Search\SearchParam;
use PHPUnit\Framework\TestCase;

class SimplePropertyTest extends TestCase
{
    protected function createObject()
    {
        $name = "teste";
        $type = PropertyType::LONG;
        $parameterName = 'teste';

        $simpleProperty = new FilterSimpleProperty($name, $type, $parameterName);

        return $simpleProperty;
    }

    public function testInvalidConstructor()
    {
        // Exception sem Properties
        $this->expectException(\InvalidArgumentException::class);
        $simpleProperty = new FilterSimpleProperty('', '', '');
    }

    public function testEmptyParameterName()
    {
        $simpleProperty = new FilterSimpleProperty("teste", PropertyType::LONG);

        $this->assertEquals("teste", $simpleProperty->getParameterName());
    }

    public function testValidConstructor()
    {
        $simpleProperty = $this->createObject();

        $this->assertEquals("teste", $simpleProperty->getName());
        $this->assertEquals(PropertyType::LONG, $simpleProperty->getType());

        $simpleProperty->setType(PropertyType::BOOL);
        $this->assertEquals(PropertyType::BOOL, $simpleProperty->getType());

        $this->assertEquals("teste", $simpleProperty->getParameterName());
    }

    public function testFilterWithoutParent()
    {
        $simpleProperty = $this->createObject();
        $result = $simpleProperty->getFilter(new SearchParam('aloha'), '');

        $this->assertEquals(
            array(
                'term' => array(
                    'teste' => 'aloha'
                )
            ),
            $result
        );
    }

    public function testFilterWithParent()
    {
        $simpleProperty = $this->createObject();
        $result = $simpleProperty->getFilter(new SearchParam('aloha'), 'parent');

        $this->assertEquals(
            array(
                'term' => array(
                    'parent.teste' => 'aloha'
                )
            ),
            $result
        );
    }
}
