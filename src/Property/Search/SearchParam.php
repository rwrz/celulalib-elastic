<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 14/04/17
 * Time: 15:27
 */

namespace CelulaLib\Elastic\Property\Search;

class SearchParam
{
    /**
     * @var array|string
     */
    protected $valor;

    /**
     * @var SearchType|int
     */
    protected $searchType;

    public function __construct($valor, int $searchType = SearchType::IGUAL_A)
    {
        if (($valor === "") || ($valor === null)) {
            throw new \InvalidArgumentException('valor obrigatório');
        }

        $this->valor = $valor;
        $this->searchType = $searchType;
    }

    /**
     * @return array|string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param array|string $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return SearchType|int
     */
    public function getSearchType()
    {
        return $this->searchType;
    }

    /**
     * @param SearchType|int $searchType
     */
    public function setSearchType($searchType)
    {
        $this->searchType = $searchType;
    }
}