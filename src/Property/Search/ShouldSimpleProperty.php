<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 12/04/17
 * Time: 17:11
 */

namespace CelulaLib\Elastic\Property\Search;

use CelulaLib\Elastic\Property\Create\SimpleProperty;

class ShouldSimpleProperty extends SimpleProperty implements ShouldInterface
{
    use TraitSearchableProperty; // Implements getParameterName, setParameterName from Searchable

    /**
     * @param string $name
     * @param $type
     * @param string $parameterName Default to $name
     */
    public function __construct(string $name, $type, $parameterName = '')
    {
        parent::__construct($name, $type);

        $this->setParameterName($parameterName);
    }

    /**
     * @param SearchParam[]|SearchParam $param
     * @param string $parentName
     * @return array
     */
    public function getShould($param, string $parentName): array
    {
        if (!empty($param)) {
            return array(
                'match' => array(
                    $this->getFullName($parentName) => array (
                        'query' => $param->getValor(),
                        'operator' => 'and'
                    )
                )
            );
        }

        return array();
    }
}