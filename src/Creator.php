<?php

namespace CelulaLib\Elastic;

use Elasticsearch\Client;

class Creator implements ICelulaElastic
{
    /**
     * @var Client
     */
    private $eClient;

    public function __construct(Client $eClient)
    {
        $this->eClient = $eClient;
    }

    public function execute()
    {
        $params = array(
            "index" => "ged",
            "body" => array(
                "settings" => array(
                    "number_of_shards" => 1,
                    "number_of_replicas" => 0
                ),
                "mappings" => array(
                    "documento" => array(
                        "dynamic_date_formats" => "yyyy-MM-dd HH:mm:ss",
                        "numeric_detection" => true,
                        "_source" => array(
                            "enabled" => true
                        ),
                        "properties" => array(
                            "codigo" => array(
                                "type" => "long"
                            ),
                            "arquivos_indexados" => array(
                                "type" => "boolean"
                            ),
                            "data_inclusao" => array(
                                "type" => "date",
                                "format" => "yyyy-MM-dd HH:mm:ss"
                            ),
                            "tipo_documento" => array(
                                "properties" => array(
                                    "codigo" => array(
                                        "type" => "long"
                                    ),
                                    "temporalidade" => array(
                                        "type" => "integer"
                                    ),
                                    "guarda_permanente" => array(
                                        "type" => "boolean"
                                    ),
                                    "descricao" => array(
                                        "type" => "string"
                                    ),
                                    "departamento" => array(
                                        "properties" => array(
                                            "codigo" => array(
                                                "type" => "long"
                                            ),
                                            "descricao" => array(
                                                "type" => "string"
                                            )
                                        )
                                    )
                                )
                            ),
                            "usuario" => array(
                                "properties" => array(
                                    "chave" => array(
                                        "type" => "long"
                                    ),
                                    "nome" => array(
                                        "type" => "string"
                                    )
                                )
                            ),
                            "arquivos" => array(
                                "type" => "nested",
                                "properties" => array(
                                    "codigo" => array(
                                        "type" => "long"
                                    ),
                                    "nome" => array(
                                        "type" => "string"
                                    ),
                                    "tamanho" => array(
                                        "type" => "long"
                                    ),
                                    "data_inclusao" => array(
                                        "type" => "date",
                                        "format" => "yyyy-MM-dd HH:mm:ss"
                                    ),
                                    "nfe" => array(
                                        "type" => "boolean"
                                    ),
                                    "extensao" => array(
                                        "properties" => array(
                                            "codigo" => array(
                                                "type" => "long"
                                            ),
                                            "nome" => array(
                                                "type" => "string"
                                            ),
                                            "icone" => array(
                                                "type" => "string"
                                            )
                                        )
                                    )
                                )
                            ),
                            "arquivo_principal" => array(
                                "properties" => array(
                                    "codigo" => array(
                                        "type" => "long"
                                    ),
                                    "nome" => array(
                                        "type" => "string"
                                    ),
                                    "tamanho" => array(
                                        "type" => "long"
                                    ),
                                    "data_inclusao" => array(
                                        "type" => "date",
                                        "format" => "yyyy-MM-dd HH:mm:ss"
                                    ),
                                    "nfe" => array(
                                        "type" => "boolean"
                                    ),
                                    "extensao" => array(
                                        "properties" => array(
                                            "codigo" => array(
                                                "type" => "long"
                                            ),
                                            "nome" => array(
                                                "type" => "string"
                                            ),
                                            "icone" => array(
                                                "type" => "string"
                                            )
                                        )
                                    )
                                )
                            ),
                            "criterios" => array(
                                "type" => "nested",
                                "properties" => array(
                                    "valor_date" => array(
                                        "type" => "date",
                                        "format" => "yyyy-MM-dd"
                                    ),
                                    "valor_boolean" => array(
                                        "type" => "boolean"
                                    ),
                                    "valor_long" => array(
                                        "type" => "long"
                                    ),
                                    "valor_float" => array(
                                        "type" => "float"
                                    ),
                                    "valor_string" => array(
                                        "type" => "string"
                                    ),
                                    "valor_keyword" => array(
                                        "type" => "keyword"
                                    ),
                                    "tipo_pasta_criterio" => array(
                                        "properties" => array(
                                            "codigo" => array(
                                                "type" => "long"
                                            ),
                                            "descricao" => array(
                                                "type" => "string"
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    "nota_fiscal" => array(
                        "dynamic_date_formats" => "yyyy-MM-dd",
                        "_source" => array(
                            "enabled" => true
                        ),
                        "properties" => array(
                            "codigo" => array(
                                "type" => "long"
                            ),
                            "nome" => array(
                                "type" => "string"
                            ),
                            "numero" => array(
                                "type" => "keyword"
                            ),
                            "chave" => array(
                                "type" => "string"
                            ),
                            "nome_fornecedor" => array(
                                "type" => "string"
                            ),
                            "cnpj_fornecedor" => array(
                                "type" => "string"
                            ),
                            "nome_empresa" => array(
                                "type" => "string"
                            ),
                            "cnpj_empresa" => array(
                                "type" => "string"
                            ),
                            "valor" => array(
                                "type" => "float"
                            ),
                            "data_emissao" => array(
                                "type" => "date",
                                "format" => "yyyy-MM-dd"
                            ),
                            "conteudo" => array(
                                "type" => "string"
                            ),
                            "codigo_cliente" => array(
                                "type" => "long"
                            )
                        )
                    )
                )
            )
        );
        $this->eClient->indices()->create($params);
    }
}