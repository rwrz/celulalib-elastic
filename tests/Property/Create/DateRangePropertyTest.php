<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 04/04/17
 * Time: 10:15
 */

namespace CelulaLibTest\Elastic\Property\Create;

use CelulaLib\Elastic\Property\Create\DateRangeProperty;
use PHPUnit\Framework\TestCase;

class DateRangePropertyTest extends TestCase
{
    public function testData()
    {
        $name = 'teste';

        $simpleProperty = new DateRangeProperty($name, $name);

        $data = [
            'teste' => [
                'type' => 'date_range',
                'format' => 'yyyy-MM-dd HH:mm:ss',
            ]
        ];

        $this->assertEquals($data, $simpleProperty->getData());
    }

    public function testInvalidFormattedValue()
    {
        $name = 'teste';
        $dateProperty = new DateRangeProperty($name, $name);

        $this->expectException(\InvalidArgumentException::class);
        $value = new \DateTime();
        $dateProperty->formatValue($value);
    }

    public function testInvalidFormattedValueCount()
    {
        $name = 'teste';
        $dateProperty = new DateRangeProperty($name, $name);

        $this->expectException(\InvalidArgumentException::class);
        $value = array(new \DateTime());
        $dateProperty->formatValue($value);
    }

    public function testFormattedValue()
    {
        $name = 'teste';
        $dateProperty = new DateRangeProperty($name, $name);

        $value = array(
            new \DateTime(),
            new \DateTime()
        );
        $result = array (
            'gte' => $value[0]->format('Y-m-d H:i:s'),
            'lte' => $value[0]->format('Y-m-d H:i:s')
        );
        $this->assertEquals($result, $dateProperty->formatValue($value));
    }
}
