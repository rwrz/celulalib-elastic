<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 06/04/17
 * Time: 15:30
 */

namespace CelulaLib\Elastic\Property\Search;

interface SearchablePropertyInterface
{
    public function getParameterName() : string;
}