<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 07/04/17
 * Time: 16:28
 */

namespace CelulaLibTest\Elastic\Property\Search;

use CelulaLib\Elastic\Property\PropertyType;
use CelulaLib\Elastic\Property\Search\SearchParam;
use CelulaLib\Elastic\Property\Search\ShouldSimpleProperty;
use PHPUnit\Framework\TestCase;

class ShouldSimplePropertyTest extends TestCase
{
    protected function createObject()
    {
        $name = "teste";
        $type = PropertyType::LONG;
        $parameterName = 'teste';

        $simpleProperty = new ShouldSimpleProperty($name, $type, $parameterName);

        return $simpleProperty;
    }

    public function testInvalidConstructor()
    {
        // Exception sem Properties
        $this->expectException(\InvalidArgumentException::class);
        $simpleProperty = new ShouldSimpleProperty('', '', '');
    }

    public function testEmptyParameterName()
    {
        $simpleProperty = new ShouldSimpleProperty("teste", PropertyType::LONG);

        $this->assertEquals("teste", $simpleProperty->getParameterName());
    }

    public function testValidConstructor()
    {
        $simpleProperty = $this->createObject();

        $this->assertEquals("teste", $simpleProperty->getName());
        $this->assertEquals(PropertyType::LONG, $simpleProperty->getType());

        $simpleProperty->setType(PropertyType::BOOL);
        $this->assertEquals(PropertyType::BOOL, $simpleProperty->getType());

        $this->assertEquals("teste", $simpleProperty->getParameterName());
    }

    public function testFilterWithoutParent()
    {
        $simpleProperty = $this->createObject();
        $result = $simpleProperty->getShould(new SearchParam('aloha'), '');

        $this->assertEquals(
            array(
                'match_phrase' => array(
                    'teste' => 'aloha'
                )
            ),
            $result
        );
    }

    public function testFilterWithParent()
    {
        $simpleProperty = $this->createObject();
        $result = $simpleProperty->getShould(new SearchParam('aloha'), 'parent');

        $this->assertEquals(
            array(
                'match_phrase' => array(
                    'parent.teste' => 'aloha'
                )
            ),
            $result
        );
    }
}
