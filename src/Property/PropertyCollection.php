<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 04/04/17
 * Time: 10:32
 */

namespace CelulaLib\Elastic\Property;

/**
 * Class PropertyCollection
 * @package CelulaLib\Elastic\Property
 */
class PropertyCollection implements PropertyCollectionInterface
{
    /**
     * @var array|PropertyInterface[]
     */
    protected $properties = array();

    /**
     * SimpleProperty constructor.
     * @param string $name
     * @param bool $nested
     */
    public function __construct(string $name, bool $nested = false)
    {
        if (empty($name)) {
            throw new \InvalidArgumentException('nome eh obrigatorio');
        }

        $this->name = $name;
        $this->nested = $nested;
    }

    /**
     * Adiciona propriedade a collection
     * @param PropertyInterface|PropertyCollectionInterface $property
     */
    public function addProperty($property)
    {
        $this->properties[] = $property;
    }

    /**
     * Limpa collection
     */
    public function clearProperties()
    {
        $this->properties = array();
    }

    public function getProperties()
    {
        return $this->properties;
    }

    public function count()
    {
        return count($this->properties);
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isNested(): bool
    {
        return $this->nested;
    }

    /**
     * @param bool $nested
     */
    public function setNested(bool $nested)
    {
        $this->nested = $nested;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        $auxProperties = array();

        foreach ($this->properties as $property) {
            $auxProperties = array_merge_recursive($auxProperties, $property->getData());
        }

        if ($this->isNested()) {
            return [
                $this->getName() => [
                    'type' => 'nested',
                    'properties' => $auxProperties
                ]
            ];
        } else {
            return [
                $this->getName() => [
                    'properties' => $auxProperties
                ]
            ];
        }
    }
}