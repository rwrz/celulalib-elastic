<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 03/04/17
 * Time: 15:23
 */

namespace CelulaLibTest\Elastic;

use CelulaLib\Elastic\Property\PropertyType;
use CelulaLib\Elastic\Property\Create\SimpleProperty;
use PHPUnit\Framework\TestCase;

class MappingTest extends TestCase
{
    public function testInvalidConstructor()
    {
        // Exception sem Properties
        $this->expectException(\InvalidArgumentException::class);
        $mapping = new \CelulaLib\Elastic\Mapping("teste", array(), array());
    }

    public function testValidateConstructor()
    {
        $name = "teste";
        $attributes = [
            "numeric_detection" => true,
        ];
        $properties = [
            new SimpleProperty('codigo', PropertyType::LONG, 'codigo')
        ];
        $mapping = new \CelulaLib\Elastic\Mapping($name, $properties, $attributes);

        $this->assertEquals($name, $mapping->getName());
        $this->assertEquals($attributes, $mapping->getAttributes());
        $this->assertEquals($properties, $mapping->getProperties());
    }

    public function testValidateDataWithInject()
    {
        $name = "teste";
        $attributes = [
            "numeric_detection" => true,
        ];
        $properties = [
            new SimpleProperty('codigo', PropertyType::LONG, 'codigo')
        ];
        $mapping = new \CelulaLib\Elastic\Mapping($name, $properties, $attributes);
        $this->assertEquals([
            'numeric_detection' => true,
            'dynamic_date_formats' => 'yyyy-MM-dd HH:mm:ss',
            '_source' => [
                'enabled' => true
            ],
            'properties' => [
                'codigo' => [
                    'type' => 'long'
                ]
            ]
        ], $mapping->getData());
    }

    public function testValidateQueryWithoutInject()
    {
        $name = "teste";
        $attributes = [
            "numeric_detection" => true,
        ];
        $properties = [
            new SimpleProperty('codigo', PropertyType::LONG, 'codigo')
        ];
        $mapping = new \CelulaLib\Elastic\Mapping($name, $properties, $attributes);
        $mapping->setShouldInjectDefaultAttributes(false);
        $this->assertEquals([
            'numeric_detection' => true,
            'properties' => [
                'codigo' => [
                    'type' => 'long'
                ]
            ]
        ], $mapping->getData());
    }

    public function testNameAndProperties()
    {
        $name = "teste";
        $attributes = [
            "numeric_detection" => true,
        ];
        $properties = [
            new SimpleProperty('codigo', PropertyType::LONG, 'codigo')
        ];
        $mapping = new \CelulaLib\Elastic\Mapping($name, $properties, $attributes);
        $this->assertEquals($name, $mapping->getName());

        $mapping->setName('teste2');
        $this->assertEquals('teste2', $mapping->getName());

        $properties[] = new SimpleProperty('codigo2', PropertyType::BOOL, 'codigo2');
        $mapping->setProperties($properties);
        $this->assertEquals($properties, $mapping->getProperties());
    }
}