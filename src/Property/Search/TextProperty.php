<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 11/05/17
 * Time: 14:23
 */
namespace CelulaLib\Elastic\Property\Search;

use CelulaLib\Elastic\Property\Create\DynamicProperty;
use CelulaLib\Elastic\Property\PropertyInterface;
use CelulaLib\Elastic\Property\PropertyType;

class TextProperty extends DynamicProperty implements
    SearchablePropertyInterface,
    FilterInterface,
    MustInterface
{
    use TraitSearchableProperty;

    /**
     * @var array|PropertyInterface[]
     */
    protected $properties = array();

    /**
     * TextProperty constructor.
     * @param string $name
     * @param string $parameterName
     */
    public function __construct(string $name, $parameterName = '')
    {
        parent::__construct($name);
        $this->setParameterName($parameterName);

        $this->addProperty(
            new FilterSimpleProperty(
                $this->getName()."_string",
                PropertyType::STRING
            )
        );

        $this->addProperty(
            new FilterSimpleProperty(
                $this->getName()."_keyword",
                PropertyType::KEYWORD
            )
        );
    }

    protected function processText($fromType, $valor, string $parentName, $searchType = SearchType::IGUAL_A)
    {
        $property = $this->properties[PropertyType::KEYWORD];
        // Esta situacao pode acontecer, no caso, validamos para trazer apenas 1
        if (is_array($valor) && ($searchType != SearchType::MULTIPLOS_VALORES)) {
            $valor = array_values($valor)[0];
        }

        if (!is_array($valor)) {
            $valor = strtoupper($valor);
        }

        if ($fromType == self::FROM_FILTER) {
            if ($searchType == SearchType::MULTIPLOS_VALORES) {
                $multiplos = array();
                foreach ($valor as $item) {
                    $multiplos[] = $item;
                }

                return array(
                    'terms' => array(
                        $property->getFullName($parentName) => $multiplos
                    )
                );
            }
        } elseif ($fromType == self::FROM_MUST) {
            switch ($searchType) {
                case SearchType::CONTEM:
                    return array(
                        'regexp' => array(
                            $property->getFullName($parentName) => '(.*' . strtoupper($valor) . '.*)'
                        )
                    );
                case SearchType::IGUAL_A:
                    return array(
                        'match' => array(
                            $property->getFullName($parentName) => $valor
                        )
                    );
                    break;
                case SearchType::COMECA_COM:
                    return array(
                        'regexp' => array(
                            $property->getFullName($parentName) => $valor . '.*'
                        )
                    );
                    break;
                case SearchType::TERMINA_COM:
                    return array(
                        'regexp' => array(
                            $property->getFullName($parentName) => '.*' . $valor
                        )
                    );
                    break;
                case SearchType::CONTEM_PALAVRA:
                    // Atenção, property STRING, nao podemos ter CASE abaixo daqui com outra property sem pegar a mesma
                    // igual aos cases superiores que são KEYWORD
                    /** @var FilterSimpleProperty $property */
                    $property = $this->properties[PropertyType::STRING];
                    return array(
                        'match' => array(
                            $property->getFullName($parentName) => array(
                                'query' => $valor,
                                'operator' => 'and'
                            )
                        )
                    );
                    break;

            }
        }

        return array();
    }

    /**
     * @param SearchParam[]|SearchParam $param
     * @param string $parentName
     * @return array
     */
    public function getFilter($param, string $parentName): array
    {
        if (!$param instanceof SearchParam) {
            $param = new SearchParam($param);
        }

        return $this->processText(
            self::FROM_FILTER,
            $param->getValor(),
            $parentName,
            $param->getSearchType()
        );
    }

    /**
     * @param SearchParam[]|SearchParam $param
     * @param string $parentName
     * @return array
     */
    public function getMust($param, string $parentName): array
    {
        if (!$param instanceof SearchParam) {
            $param = new SearchParam($param);
        }

        return $this->processText(
            self::FROM_MUST,
            $param->getValor(),
            $parentName,
            $param->getSearchType()
        );
    }
}