<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 06/04/17
 * Time: 15:35
 */

namespace CelulaLib\Elastic\Property\Search;

final class SearchableType
{
    const MUST      = 1;
    const FILTER    = 2;
    const SHOULD    = 4;

    // ensures that this class acts like an enum
    // and that it cannot be instantiated
    public function __construct()
    {
        throw new \Exception('ENUMs can\'t be instantiated');
    }
}