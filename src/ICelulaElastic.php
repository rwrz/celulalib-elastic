<?php

namespace CelulaLib\Elastic;

interface ICelulaElastic
{
    public function execute();
}