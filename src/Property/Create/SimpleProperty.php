<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 30/03/17
 * Time: 11:46
 */

namespace CelulaLib\Elastic\Property\Create;

use CelulaLib\Elastic\Property\PropertyType;

class SimpleProperty extends AbstractProperty implements FormattedValueInterface
{
    /**
     * @var string|PropertyType
     */
    protected $type;

    /**
     * SimpleProperty constructor.
     * @param string $name
     * @param $type
     */
    public function __construct(string $name, $type)
    {
        parent::__construct($name);
        if (empty($type)) {
            throw new \InvalidArgumentException('nome e tipo sao obrigatorios');
        }
        $this->type = $type;
    }

    /**
     * @return PropertyType|string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param PropertyType|string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function getData(): array
    {
        return array (
            $this->getName() => array (
                "type" => $this->getType()
            )
        );
    }

    /**
     * Formata o valor na hora do insert/update
     * @param $value mixed
     * @return mixed|string
     */
    public function formatValue($value)
    {
        switch ($this->getType()) {
            case PropertyType::LONG:
                return (int)$value;
            case PropertyType::FLOAT:
                return (float)str_replace(',', '.', $value);
            case PropertyType::BOOL:
                return (bool)$value;
            case PropertyType::STRING:
                if (!is_array($value)) {
                    return strtoupper($value);
                }
                break;
        }

        return $value;
    }
}