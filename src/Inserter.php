<?php

namespace CelulaLib\Elastic;

use Elasticsearch\Client;
use GEDDigital\Entity\Documento as DocumentoEntity;

class Inserter extends AbstractCelulaElastic
{
    public function __construct(Client $eClient, $documentos)
    {
        $this->eClient = $eClient;
        $this->documentos = $documentos;
    }

    public function execute()
    {
        /** @var DocumentoEntity $documento */
        foreach ($this->documentos as $documento) {
            $this->params = array(
                "index" => "ged",
                "type" => "documento",
                "id" => $documento->getCodigo(),
                "body" => array(
                    "codigo" => (int) $documento->getCodigo(),
                    "arquivos_indexados" => false,
                    "data_inclusao" => $documento->getDataInclusao()->format('Y-m-d H:i:s'),
                    "tipo_documento" => array(
                        "codigo" => $documento->getTipoDocumento()->getCodigo(),
                        "temporalidade" => $documento->getTipoDocumento()->getTemporalidade(),
                        "guarda_permanente" => $documento->getTipoDocumento()->getGuardaPermanente(),
                        "descricao" => $documento->getTipoDocumento()->getDescricao(),
                        "departamento" => array(
                            "codigo" => $documento->getTipoDocumento()->getDepartamento()->getCodigo(),
                            "descricao" => $documento->getTipoDocumento()->getDepartamento()->getDescricao(),
                        )
                    ),
                    "usuario" => array(
                        "chave" => (int) $documento->getUsuario()->getChave(),
                        "nome" => $documento->getUsuario()->getNome()
                    )
                )
            );
            $this->processCriterios($documento);
            parent::execute();
        }
    }
}