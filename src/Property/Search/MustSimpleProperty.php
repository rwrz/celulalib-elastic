<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 14/04/17
 * Time: 12:22
 */

namespace CelulaLib\Elastic\Property\Search;

use CelulaLib\Elastic\Property\Create\SimpleProperty;
use CelulaLib\Elastic\Property\PropertyType;

class MustSimpleProperty extends SimpleProperty implements MustInterface
{
    use TraitSearchableProperty; // Implements getParameterName, setParameterName from Searchable

    /**
     * @param string $name
     * @param $type
     * @param string $parameterName Default to $name
     */
    public function __construct(string $name, $type, $parameterName = '')
    {
        parent::__construct($name, $type);

        $this->setParameterName($parameterName);
    }

    /**
     * @param SearchParam[]|SearchParam $param
     * @param string $parentName
     * @return array
     */
    public function getMust($param, string $parentName): array
    {
        $valor = $param->getValor();
        $begin = $valor;
        if (is_array($valor)) {
            $tmp = array_values($valor);
            $begin = $tmp[0];
        }

        switch ($this->getType()) {
            case PropertyType::KEYWORD:
                switch ($param->getSearchType()) {
                    case SearchType::IGUAL_A:
                        return array(
                            'match' => array(
                                $this->getFullName($parentName) => strtoupper($begin)
                            )
                        );
                    case SearchType::COMECA_COM:
                        return array(
                            'regexp' => array(
                                $this->getFullName($parentName) => strtoupper($begin).'.*'
                            )
                        );
                    case SearchType::TERMINA_COM:
                        return array(
                            'regexp' => array(
                                $this->getFullName($parentName) => '.*'.strtoupper($begin)
                            )
                        );
                }
                break;
            case PropertyType::STRING:
                switch ($param->getSearchType()) {
                    case SearchType::CONTEM:
                        return array(
                            'match' => array(
                                $this->getFullName($parentName) => strtoupper($begin)
                            )
                        );
                }
                break;
        }

        return array();
    }
}
