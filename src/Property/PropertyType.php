<?php
/**
 * Celula Gestão de Arquivos e Documentos
 * User: rboratto
 * Date-time: 16/10/12 14:23
 */

namespace CelulaLib\Elastic\Property;

final class PropertyType
{
    const LONG      = "long";
    const INTEGER   = "integer";
    const BOOL      = "boolean";
    const STRING    = "string";
    const KEYWORD   = "keyword";
    const FLOAT     = "float";
    const DATE      = "date";
    const DATE_RANGE= "date_range";
    const LONG_RANGE= "long_range";

    // ensures that this class acts like an enum
    // and that it cannot be instantiated
    public function __construct()
    {
        throw new \Exception('ENUMs can\'t be instantiated');
    }
}