<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 04/04/17
 * Time: 10:15
 */

namespace CelulaLibTest\Elastic\Property\Create;

use CelulaLib\Elastic\Property\PropertyType;
use CelulaLib\Elastic\Property\Create\DateProperty;
use PHPUnit\Framework\TestCase;

class DatePropertyTest extends TestCase
{
    public function testInvalidConstructor()
    {
        // Exception sem Properties
        $this->expectException(\InvalidArgumentException::class);
        $simpleProperty = new DateProperty('', '');
    }

    public function testTrySetType()
    {
        $simpleProperty = new DateProperty('alow', 'alow');
        $this->expectException(\Exception::class);
        $simpleProperty->setType(PropertyType::BOOL);
    }

    public function testValidConstructor()
    {
        $name = 'teste';
        $type = 'date';

        $simpleProperty = new DateProperty($name, $name);

        $this->assertEquals($name, $simpleProperty->getName());
        $this->assertEquals($type, $simpleProperty->getType());
    }

    public function testData()
    {
        $name = 'teste';

        $simpleProperty = new DateProperty($name, $name);

        $data = [
            'teste' => [
                'type' => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss'
            ]
        ];

        $this->assertEquals($data, $simpleProperty->getData());
    }

    public function testFormattedValue()
    {
        $name = 'teste';
        $dateProperty = new DateProperty($name, $name);

        $value = new \DateTime();
        $this->assertEquals($dateProperty->formatValue($value), $value->format('Y-m-d H:i:s'));

        $value = new \DateTime();
        $this->assertEquals($dateProperty->formatValue($value->format('Y-m-d H:i:s')), $value->format('Y-m-d H:i:s'));
    }
}
