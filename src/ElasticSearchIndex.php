<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 04/04/17
 * Time: 11:27
 */

namespace CelulaLib\Elastic;

use CelulaLib\Elastic\Property\PropertyCollection;
use CelulaLib\Elastic\Property\PropertyCollectionInterface;
use CelulaLib\Elastic\Property\Search\SearchablePropertyInterface;
use CelulaLib\Elastic\Property\Search\FilterInterface;
use CelulaLib\Elastic\Property\Search\MustInterface;
use CelulaLib\Elastic\Property\Search\SearchParam;
use CelulaLib\Elastic\Property\Search\ShouldInterface;
use Elasticsearch\Client;
use PhpConsole\Handler;

class ElasticSearchIndex
{
    /**
     * @var string
     */
    private $name;

    /**
     * Client do ElasticSearch
     * @var Client
     */
    private $elasticSearchClient;

    /**
     * Configuracoes do Client
     * @var array
     */
    private $settings = array(
        "number_of_shards" => 1,
        "number_of_replicas" => 0
    );

    /**
     * @var array|Mapping[]
     */
    private $mappings = array();

    /**
     * @var array
     */
    private $query = array();

    /**
     * Should hightligh searched fields ?
     * @var bool
     */
    private $highlight = true;

    private $highlightKey = 1;

    /**
     * Highlight pre tag
     * @var array
     */
    private $highlight_pre_tags = array("<span class='w-700 text-danger'>");

    /**
     * Highlight post tag
     * @var array
     */
    private $highlight_post_tags = array("</span>");

    /**
     * ElasticSearchIndex constructor.
     * @param string $name
     * @param Mapping[] $mappings
     * @param Client $elasticSearchClient
     */
    public function __construct(string $name, array $mappings, Client $elasticSearchClient)
    {
        if (empty($name) || empty($mappings) || empty($elasticSearchClient)) {
            throw new \InvalidArgumentException("nome, mappings e elasticsearchclient sao parametros obrigatorios");
        }

        $this->setName($name);
        $this->elasticSearchClient = $elasticSearchClient;
        $this->setMappings($mappings);
    }

    /**
     * Cria os indices passados
     * @param bool $force Deleta e cria um novo caso o indice ja exista
     * @return array|bool
     * @throws \Exception
     */
    public function create($force = false)
    {
        $params = array (
            "index" => $this->getName(),
            "body" => array(
                "settings" => $this->getSettings(),
                "mappings" => $this->formatMappings()
            )
        );

        if (!$this->exists($params)) {
            return $this->getElasticSearchClient()->indices()->create($params);
        } elseif ($force) {
            $this->delete();
            return $this->getElasticSearchClient()->indices()->create($params);
        }

        throw new \Exception("nao foi possivel criar, indice ja existente, execute com force = true");
    }

    /**
     * @param PropertyCollectionInterface $property
     * @param array $data
     * @return array
     */
    protected function proccessPropertyCollection(PropertyCollectionInterface $property, array $data)
    {
        $aux = array();
        foreach ($property->getProperties() as $property) {
            // temos a propriedade no data?
            if (key_exists($property->getName(), $data)) {
                // Se for collection, recursive
                if (($property instanceof PropertyCollection)) {
                    // Se for nested
                    if ($property->isNested()) {
                        foreach ($data[$property->getName()] as $tmpData) {
                            $aux[$property->getName()][] = $this->proccessPropertyCollection(
                                $property,
                                $tmpData
                            );
                        }
                    } else {
                        $aux[$property->getName()] = $this->proccessPropertyCollection(
                            $property,
                            $data[$property->getName()]
                        );
                    }
                } else {
                    // Pega o valor em DATA, caso precise de format, formatamos
                    $value = $data[$property->getName()];

                    $aux = array_merge_recursive($aux, $property->getValue($value));
                }
            }
        }

        return $aux;
    }

    /**
     * Verifica se existe o indice
     * @return bool
     */
    public function exists()
    {
        return $this->getElasticSearchClient()->indices()->exists(array('index' => $this->getName()));
    }

    /**
     * Deleta o indice
     */
    public function delete()
    {
        $this->getElasticSearchClient()->indices()->delete(array('index' => $this->getName()));
    }

    /**
     * @param string $mappingName
     * @param string $id ID do indice deste mapping, uma chave unica para o mapping
     * @param mixed $data
     * @return array|bool
     * @throws \Exception
     */
    public function insert(string $mappingName, string $id, $data)
    {
        $this->indexData($mappingName, $id, $data);
    }

    /**
     * @param string $mappingName
     * @param string $id ID do indice deste mapping, uma chave unica para o mapping
     * @param mixed $data
     * @return array|bool
     * @throws \Exception
     */
    public function update(string $mappingName, string $id, $data)
    {
        $this->indexData($mappingName, $id, $data, true);
    }

    /**
     * @param string $mappingName
     * @param string $id
     * @param $data
     * @param bool $update
     * @return array
     * @throws \Exception
     */
    private function indexData(string $mappingName, string $id, $data, $update = false)
    {
        foreach ($this->getMappings() as $mapping) {
            if ($mapping->getName() == $mappingName) {
                // Encontramos o mapping que desejamos fazer insert
                // Agora vamos montar a estrutura
                // Primeiro, o BODY. vamos mapear o array $data com as propriedades que temos no indice
                $data = $mapping->convertDataToInsert($data);
                $body = $this->proccessPropertyCollection($mapping, $data);

                $params = [
                    "index" => $this->getName(),
                    "type" => $mapping->getName(),
                    "id" => $id,
                    "body" => ($update ? ['doc' => $body] : $body)
                ];

                //print_r($params);

                if ($update) {
                    try {
                        return $this->getElasticSearchClient()->update($params);
                    } catch (\Exception $exp) {
                        $json = json_decode($exp->getMessage());
                        if (isset($json->error) && isset($json->error->type)
                            && ($json->error->type == "document_missing_exception")) {
                            // deu erro de document missing, fazemos o insert
                            $aux = $params['body']['doc'];
                            unset($params['body']['doc']);
                            $params['body'] = $aux;
                            return $this->getElasticSearchClient()->index($params);
                        } else {
                            throw $exp;
                        }
                    }
                } else {
                    // Insere no ElasticSearch
                    return $this->getElasticSearchClient()->index($params);
                }
            }
        }

        throw new \Exception("mapping nao existente [".$mappingName."]");
    }

    /**
     * @param string $mappingName
     * @return bool|Mapping
     */
    protected function getMapping(string $mappingName)
    {
        foreach ($this->getMappings() as $mapping) {
            if ($mapping->getName() == $mappingName) {
                return $mapping;
            }
        }

        return false;
    }

    /**
     * @param PropertyCollectionInterface $property
     * @param SearchParam[] $parametros
     * @param string $fullParentName
     * @return array $resultQuery
     */
    protected function proccessPropertyCollectionForQuery(
        PropertyCollectionInterface $property,
        array $parametros,
        string $fullParentName
    ) {
        $resultHighlightFields = array();
        $resultQuery = array();
        $parentName = "";
        // Em caso de mapping, parent é vazio
        if (!empty($fullParentName)) {
            $parentName = $fullParentName . "." . $property->getName();
        } elseif (!$property instanceof Mapping) {
            $parentName = $property->getName();
        }

        foreach ($property->getProperties() as $propertyAux) {
            // Se for collection, e não implementar SearchablePropertyInterface, pega as filhas e percorre
            if ($propertyAux instanceof PropertyCollection) {
                if ($propertyAux->isNested()) {
                    // Se for nested, temos que tratar a busca um pouco diferente
                    $queries = $this->proccessPropertyCollectionForQuery(
                        $propertyAux,
                        $parametros,
                        $parentName
                    );

                    // Se tivemos matchs, parameterName com parametros
                    if (!empty($queries['queries'])) {
                        $auxResultQuery = array();
                        foreach ($queries['queries'] as $query) {
                            $tmp = [
                                'nested' => [
                                    'path' => $propertyAux->getName(),
                                ]
                            ];

                            if ($this->isHighlight()) {
                                $highlightKey = $this->getNextHighlightKey();
                                $tmp['nested']['inner_hits'] = array (
                                    'name' => (string)$highlightKey,
                                    'highlight' => array (
                                        'pre_tags' => $this->getHighlightPreTags(),
                                        'post_tags' => $this->getHighlightPostTags(),
                                        'fields' => $queries['highlightFields']
                                    )
                                );
                            }

                            $tmp = array_merge_recursive($tmp, [
                                'nested' => $query
                            ]);

                            if (!empty($tmp['nested']['query']['bool']['should'])) {
                                $auxResultQuery['query']['bool']['should'][] = $tmp;
                                $auxResultQuery['query']['bool']['minimum_should_match'] = 1;
                            } else {
                                $auxResultQuery['query']['bool']['must'][] = $tmp;
                            }
                        }
                        $resultQuery = array_merge_recursive(
                            $resultQuery,
                            array($auxResultQuery)
                        );
                    }
                } else {
                    $queries = $this->proccessPropertyCollectionForQuery(
                        $propertyAux,
                        $parametros,
                        $parentName
                    );
                    $resultQuery = array_merge_recursive(
                        $resultQuery,
                        $queries['queries']
                    );
//                    foreach ($queries['queries'] as $query) {
//                        $resultQuery = array_merge_recursive(
//                            $resultQuery,
//                            $query
//                        );
//                    }
                    if ($this->isHighlight()) {
                        $resultHighlightFields = array_merge_recursive(
                            $resultHighlightFields,
                            $queries['highlightFields']
                        );
                    }
                }
            } elseif ($propertyAux instanceof SearchablePropertyInterface) {
                // Propriedade é pesquisável ?
                // e temos a propriedade nos parametros ?
                if (array_key_exists($propertyAux->getParameterName(), $parametros)) {
                    // Testa ela, e para onde vai

                    $param = $parametros[$propertyAux->getParameterName()];

                    if (is_array($param)) {
                        $param = array_values($param);
                    }

                    // Param está vazio ? pois não validamos parametros vazios
                    if ($param != "") {
                        if (((!$param instanceof SearchParam) && is_array($param) && (count($param) > 0)
                                && (!$param[0] instanceof SearchParam))
                            || ((!$param instanceof SearchParam) && (!is_array($param)))
                        ) {
                            // Parâmetros do tipo TEXTO|NUM|BOOL|etc sem implementar o SearchParam
                            $param = new SearchParam($param);
                        }

                        // Forca um array
                        if (!is_array($param)) {
                            $params = array($param);
                        } else {
                            $params = $param;
                        }

                        $auxResultQuery = array();
                        foreach ($params as $param) {
                            $auxQuery = array();
                            if ($propertyAux instanceof MustInterface) {
                                $tmp = $propertyAux->getMust(
                                    $param,
                                    $parentName
                                );
                                if (!empty($tmp)) {
                                    $auxQuery['query']['bool']['must'][] = $tmp;
                                }
                            }

                            if ($propertyAux instanceof FilterInterface) {
                                $tmp = $propertyAux->getFilter(
                                    $param,
                                    $parentName
                                );
                                if (!empty($tmp)) {
                                    $auxQuery['query']['bool']['filter']['bool']['must'][] = $tmp;
                                }
                            }

                            if ($propertyAux instanceof ShouldInterface) {
                                $tmp = $propertyAux->getShould(
                                    $param,
                                    $parentName
                                );

                                if (!empty($tmp)) {
                                    $auxQuery['query']['bool']['should'][] = $tmp;
                                }
                            }

                            if (!empty($auxQuery)) {
                                // Add query to array
                                $auxResultQuery[] = $auxQuery;
                            }
                        }

                        if (!empty($auxResultQuery)) {
                            // Get field name for highlight
                            $resultHighlightFields = array_merge_recursive(
                                $resultHighlightFields,
                                $propertyAux->getHighlight($parentName)
                            );
                        }

                        // Estamos retornando para um collection ou para root ?
                        // root = empty
//                        if (empty($parentName)) {
//                            foreach ($auxResultQuery as $query) {
//                                $resultQuery = array_merge_recursive(
//                                    $resultQuery,
//                                    $query
//                                );
//                            }
//                        } else {
                            $resultQuery = array_merge_recursive(
                                $resultQuery,
                                $auxResultQuery
                            );
//                        }
                    }
                }
            }
        }

        return array(
            'queries' => $resultQuery,
            'highlightFields' => $resultHighlightFields
        );
    }

    /**
     * @param string $mappingName
     * @param array $parametros
     * @param string $scroll
     * @param int $size
     * @return array
     * @throws \Exception
     */
    public function getQuery(string $mappingName, array $parametros, $scroll = '5m', $size = 5)
    {
        if (!$mapping = $this->getMapping($mappingName)) {
            throw new \Exception("Mapping [".$mappingName."] não existe.");
        }

        $bodyQuery = $this->proccessPropertyCollectionForQuery($mapping, $parametros, "");

        $query = [
            'query' => [
                "match_all" => new \stdClass()
            ]
        ];
        if (!empty($bodyQuery['queries'])) {
            $query = array ();
            foreach ($bodyQuery['queries'] as $queryTmp) {
                $query = array_merge_recursive($query, $queryTmp);
            }
        }

        /** Template da Query */
        $this->query = array(
            'index' => $this->getName(),
            'type'  => $mapping->getName(),
            'size'  => $size,
            'scroll' => $scroll,
            'body'  => array(
                'query' => $query['query'],
                'highlight' => array (
                    'fields' => $bodyQuery['highlightFields']
                ),
                'sort'  => array(
                    '_score' => array(
                        'order' => 'desc'
                    )
                )
            )
        );

        Handler::getInstance()->debug(json_encode($this->query['body']), 'ELASTIC_QUERY');
        //echo "\n".json_encode($this->query['body'])."\n";

        return $this->query;
    }

    /**
     * @param string $mappingName
     * @param array $parametros
     * @param string $scroll
     * @param int $size
     * @return array
     */
    public function search(string $mappingName, array $parametros, $scroll = '5m', $size = 5)
    {
        return $this->getElasticSearchClient()->search($this->getQuery($mappingName, $parametros, $scroll, $size));
    }

    /**
     * @param $scrollId
     * @param string $scroll
     * @return array
     */
    public function scroll($scrollId, $scroll = '5m')
    {
        return $this->getElasticSearchClient()->scroll([
            'scroll_id' => $scrollId,
            'scroll'    => $scroll
        ]);
    }

    /**
     * @return array
     */
    protected function formatMappings()
    {
        $aux = array();

        foreach ($this->getMappings() as $mapping) {
            $aux[$mapping->getName()] = $mapping->getData();
        }

        return $aux;
    }

    /**
     * @return array|Mapping[]
     */
    public function getMappings()
    {
        return $this->mappings;
    }

    /**
     * @param array|Mapping[] $mappings
     */
    public function setMappings($mappings)
    {
        $this->mappings = $mappings;
    }

    /**
     * @return Client
     */
    public function getElasticSearchClient(): Client
    {
        return $this->elasticSearchClient;
    }

    /**
     * @return array
     */
    public function getSettings(): array
    {
        return $this->settings;
    }

    /**
     * @param array $settings
     */
    public function setSettings(array $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Increments highlight key and returns to current one
     * @return int
     */
    protected function getNextHighlightKey()
    {
        return $this->highlightKey++;
    }

    /**
     * @return bool
     */
    public function isHighlight(): bool
    {
        return $this->highlight;
    }

    /**
     * @param bool $highlight
     */
    public function setHighlight(bool $highlight)
    {
        $this->highlight = $highlight;
    }

    /**
     * @return array
     */
    public function getHighlightPreTags(): array
    {
        return $this->highlight_pre_tags;
    }

    /**
     * @param array $highlight_pre_tags
     */
    public function setHighlightPreTags(array $highlight_pre_tags)
    {
        $this->highlight_pre_tags = $highlight_pre_tags;
    }

    /**
     * @return array
     */
    public function getHighlightPostTags(): array
    {
        return $this->highlight_post_tags;
    }

    /**
     * @param array $highlight_post_tags
     */
    public function setHighlightPostTags(array $highlight_post_tags)
    {
        $this->highlight_post_tags = $highlight_post_tags;
    }
}
