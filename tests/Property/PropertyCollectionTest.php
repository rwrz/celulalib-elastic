<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 04/04/17
 * Time: 10:38
 */

namespace CelulaLibTest\Elastic\Property;

use CelulaLib\Elastic\Property\PropertyType;
use CelulaLib\Elastic\Property\PropertyCollection;
use CelulaLib\Elastic\Property\Create\SimpleProperty;
use PHPUnit\Framework\TestCase;

class PropertyCollectionTest extends TestCase
{
    public function testInvalidConstructor()
    {
        // Exception sem Properties
        $this->expectException(\InvalidArgumentException::class);
        $obj = new PropertyCollection('');
    }

    public function testValidConstructor()
    {
        $name = 'teste';
        $obj = new PropertyCollection($name);

        $this->assertEquals($name, $obj->getName());
    }

    public function testCollection()
    {
        $name = 'teste';
        $obj = new PropertyCollection($name);
        $testeA = new SimpleProperty('testeA', PropertyType::LONG, 'testeA');
        $testeB = new SimpleProperty('testeB', PropertyType::BOOL, 'testeB');
        $obj->addProperty($testeA);
        $obj->addProperty($testeB);

        $data = array (
            $name => array (
                'properties' => array (
                    'testeA' => array (
                        "type" => 'long'
                    ),
                    'testeB' => array (
                        "type" => 'boolean'
                    ),
                )
            )
        );

        $this->assertEquals($data, $obj->getData());
    }

    public function testNestedCollection()
    {
        $name = 'teste';
        $obj = new PropertyCollection($name, true);
        $testeA = new SimpleProperty('testeA', PropertyType::LONG, 'testeA');
        $testeB = new SimpleProperty('testeB', PropertyType::BOOL, 'testeB');
        $obj->addProperty($testeA);
        $obj->addProperty($testeB);

        $data = array (
            $name => array (
                'type' => 'nested',
                'properties'  => array (
                    'testeA' => array (
                        "type" => 'long'
                    ),
                    'testeB' => array (
                        "type" => 'boolean'
                    ),
                )
            )
        );

        $this->assertEquals($data, $obj->getData());
    }

    public function testCountClearAndNested()
    {
        $name = 'teste';
        $obj = new PropertyCollection($name);
        $testeA = new SimpleProperty('testeA', PropertyType::LONG, 'testeA');
        $testeB = new SimpleProperty('testeB', PropertyType::BOOL, 'testeB');
        $obj->addProperty($testeA);
        $obj->addProperty($testeB);

        $this->assertEquals(2, $obj->count());
        $this->assertEquals(2, count($obj->getProperties()));
        $obj->clearProperties();
        $this->assertEquals(0, $obj->count());

        $obj->setNested(true);
        $this->assertTrue($obj->isNested());
    }

    public function testCollectionWithCollection()
    {
        $name = 'teste';
        $obj = new PropertyCollection($name, true);
        $testeA = new SimpleProperty('testeA', PropertyType::LONG, 'testeA');
        $testeB = new SimpleProperty('testeB', PropertyType::BOOL, 'testeB');
        $obj->addProperty($testeA);
        $obj->addProperty($testeB);

        $name = 'teste2';
        $obj2 = new PropertyCollection($name, true);
        $obj2->addProperty($testeA);
        $obj2->addProperty($testeB);

        $obj->addProperty($obj2);

        $data = array (
            'teste' => array (
                'type' => 'nested',
                'properties'  => array (
                    'testeA' => array (
                        "type" => 'long'
                    ),
                    'testeB' => array (
                        "type" => 'boolean'
                    ),
                    'teste2' => array (
                        'type' => 'nested',
                        'properties' => array (
                            'testeA' => array (
                                "type" => 'long'
                            ),
                            'testeB' => array (
                                "type" => 'boolean'
                            ),
                        )
                    )
                )
            )
        );

        $this->assertEquals($data, $obj->getData());
    }
}
