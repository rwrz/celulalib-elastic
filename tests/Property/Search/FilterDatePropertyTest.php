<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 04/04/17
 * Time: 10:15
 */

namespace CelulaLibTest\Elastic\Property\Create;

use CelulaLib\Elastic\Property\Create\DateProperty;
use CelulaLib\Elastic\Property\Search\FilterDateProperty;
use CelulaLib\Elastic\Property\Search\SearchParam;
use PHPUnit\Framework\TestCase;

class FilterDatePropertyTest extends TestCase
{
    public function testInvalidConstructor()
    {
        // Exception sem Properties
        $this->expectException(\InvalidArgumentException::class);
        $simpleProperty = new FilterDateProperty('', '');
    }

    public function testValidConstructor()
    {
        $name = 'teste';
        $type = 'date';

        $simpleProperty = new DateProperty($name, $name);

        $this->assertEquals($name, $simpleProperty->getName());
        $this->assertEquals($type, $simpleProperty->getType());
    }

    public function testEmptyParameterName()
    {
        $simpleProperty = new FilterDateProperty("teste");

        $this->assertEquals("teste", $simpleProperty->getParameterName());

        $simpleProperty = new FilterDateProperty("teste", "teste2");

        $this->assertEquals("teste2", $simpleProperty->getParameterName());
    }

    public function testFilter()
    {
        $data = new \DateTime();
        $params = array (
            'inicio' => $data,
            'fim' => $data->add(new \DateInterval("P2D"))
        );
        $dateProperty = new FilterDateProperty('teste');
        $result = $dateProperty->getFilter(new SearchParam($params), '');
        $expected = array (
            'range' => array (
                'teste' => array(
                    'gte'    => $params['inicio']->format("Y-m-d"),
                    'lte'    => $params['fim']->format("Y-m-d"),
                    'format' => 'yyyy-MM-dd'
                )
            )
        );

        $this->assertEquals($expected, $result);
    }

    public function testFilterArrayOneValue()
    {
        $data = new \DateTime();
        $params = array (
            'inicio' => $data
        );
        $dateProperty = new FilterDateProperty('teste');
        $result = $dateProperty->getFilter(new SearchParam($params), '');
        $expected = array (
            'range' => array (
                'teste' => array(
                    'gte'    => $params['inicio']->format("Y-m-d"),
                    'lte'    => $params['inicio']->format("Y-m-d"),
                    'format' => 'yyyy-MM-dd'
                )
            )
        );

        $this->assertEquals($expected, $result);
    }
}
