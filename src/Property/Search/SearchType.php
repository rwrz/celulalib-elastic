<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 14/04/17
 * Time: 12:10
 */
namespace CelulaLib\Elastic\Property\Search;

final class SearchType
{
    const IGUAL_A       = 1;
    const COMECA_COM    = 2;
    const CONTEM        = 3;
    const TERMINA_COM   = 4;
    const MAIOR_QUE     = 5;
    const MENOR_QUE     = 6;
    const NO_INTERVALO  = 7;
    const MULTIPLOS_VALORES  = 8;
    const CONTEM_PALAVRA= 9;

    // ensures that this class acts like an enum
    // and that it cannot be instantiated
    public function __construct()
    {
        throw new \Exception('ENUMs can\'t be instantiated');
    }
}
