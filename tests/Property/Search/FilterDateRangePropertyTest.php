<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 09/05/17
 * Time: 16:07
 */

namespace CelulaLibTest\Elastic\Property\Search;

use CelulaLib\Elastic\Property\Search\FilterDateRangeProperty;
use CelulaLib\Elastic\Property\Search\SearchParam;
use PHPUnit\Framework\TestCase;

class FilterDatePropertyTest extends TestCase
{
    public function testInvalidConstructor()
    {
        // Exception sem Properties
        $this->expectException(\InvalidArgumentException::class);
        $simpleProperty = new FilterDateRangeProperty('', '');
    }

    public function testValidConstructor()
    {
        $name = 'teste';
        $parameterName = 'teste_par';

        $simpleProperty = new FilterDateRangeProperty($name, $parameterName);

        $this->assertEquals($name, $simpleProperty->getName());
        $this->assertEquals($parameterName, $simpleProperty->getParameterName());
    }

    public function testEmptyParameterName()
    {
        $simpleProperty = new FilterDateRangeProperty("teste");

        $this->assertEquals("teste", $simpleProperty->getParameterName());

        $simpleProperty = new FilterDateRangeProperty("teste", "teste2");

        $this->assertEquals("teste2", $simpleProperty->getParameterName());
    }

    public function testFilter()
    {
        $data = new \DateTime();
        $params = array (
            'inicio' => $data,
            'fim' => $data->add(new \DateInterval("P2D"))
        );
        $dateProperty = new FilterDateRangeProperty('teste');
        $result = $dateProperty->getFilter(new SearchParam($params), '');
        $expected = array (
            'range' => array (
                'teste' => array(
                    'gte'    => $params['inicio']->format("Y-m-d"),
                    'lte'    => $params['fim']->format("Y-m-d"),
                    'format' => 'yyyy-MM-dd',
                    'relation' => 'intersects'
                )
            )
        );

        $this->assertEquals($expected, $result);
    }

    public function testFilterArrayOneValue()
    {
        $data = new \DateTime();
        $params = array (
            'inicio' => $data
        );
        $dateProperty = new FilterDateRangeProperty('teste');
        $result = $dateProperty->getFilter(new SearchParam($params), '');
        $expected = array (
            'range' => array (
                'teste' => array(
                    'gte'    => $params['inicio']->format("Y-m-d"),
                    'lte'    => $params['inicio']->format("Y-m-d"),
                    'format' => 'yyyy-MM-dd',
                    'relation' => 'intersects'
                )
            )
        );

        $this->assertEquals($expected, $result);
    }
}
