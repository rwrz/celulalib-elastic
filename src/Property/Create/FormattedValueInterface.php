<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 04/04/17
 * Time: 15:03
 */

namespace CelulaLib\Elastic\Property\Create;

interface FormattedValueInterface
{
    /**
     * Formata o valor na hora do insert/update
     * @param $value mixed
     * @return mixed|string
     */
    public function formatValue($value);
}