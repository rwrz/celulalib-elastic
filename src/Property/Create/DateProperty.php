<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 03/04/17
 * Time: 16:55
 */

namespace CelulaLib\Elastic\Property\Create;

use CelulaLib\Elastic\Property\Create\FormattedValueInterface;
use CelulaLib\Elastic\Property\Create\SimpleProperty;

class DateProperty extends SimpleProperty implements FormattedValueInterface
{
    /**
     * SimpleProperty constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        parent::__construct($name, 'date');
    }

    public function setType($type)
    {
        throw new \Exception("nao e possivel alterar tipo da propriedade data");
    }

    public function getData(): array
    {
        return array (
            $this->getName() => array (
                "type" => $this->getType(),
                'format' => 'yyyy-MM-dd HH:mm:ss'
            )
        );
    }

    public function fixDate($value)
    {
        if (is_string($value)) {
            try {
                $data = preg_split("/[-\/]/", $value);
                if (count($data) == 3) { // valid date!
                    if (strlen($data[0]) == 2) { // temos dd/mm/yyyy em teoria
                        $value = new \DateTime($data[2] . "-" . $data[1] . "-" . $data[0]);
                    } else { // temos yyyy-mm-dd em teoria
                        $value = new \DateTime($data[0] . "-" . $data[1] . "-" . $data[2]);
                    }
                }
            } catch (\Exception $exp) {
                return $value;
            }
        }

        return $value;
    }

    /**
     * Formata o valor na hora do insert/update
     * @param $value mixed
     * @return mixed|string
     */
    public function formatValue($value)
    {
        if (is_array($value)) {
            $value = array_values($value)[0];
        }

        $value = $this->fixDate($value);

        if ($value instanceof \DateTime) {
            return $value->format('Y-m-d H:i:s');
        }

        return $value;
    }
}