<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 09/05/17
 * Time: 15:08
 */

namespace CelulaLib\Elastic\Property\Create;

use CelulaLib\Elastic\Property\PropertyType;

class LongRangeProperty extends SimpleProperty implements FormattedValueInterface
{
    /**
     * DateRangeProperty constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        parent::__construct($name, PropertyType::LONG_RANGE);
    }

    /**
     * Formata o valor na hora do insert/update
     * @param $value mixed
     * @return mixed|string
     */
    public function formatValue($value)
    {
        if (!is_array($value) || (count($value) < 2)) {
            throw new \InvalidArgumentException('Long Range exige um array para inserir, 
                e o mesmo deve possuir 2 elementos');
        }

        $begin = $value[0]+0;
        $end = $value[1]+0;

        if ($begin > $end) {
            $aux = $begin;
            $begin = $end;
            $end = $aux;
        }

        $return = [];
        $return['gte'] = $begin;
        $return['lte'] = $end;

        return $return;
    }
}