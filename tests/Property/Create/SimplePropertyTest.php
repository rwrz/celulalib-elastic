<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 03/04/17
 * Time: 15:23
 */

namespace CelulaLibTest\Elastic\Property\Create;

use CelulaLib\Elastic\Property\PropertyType;
use CelulaLib\Elastic\Property\Create\SimpleProperty;
use PHPUnit\Framework\TestCase;

class SimplePropertyTest extends TestCase
{
    protected function createObject()
    {
        $name = "teste";
        $type = PropertyType::LONG;

        $simpleProperty = new SimpleProperty($name, $type);

        return $simpleProperty;
    }

    public function testInvalidConstructor()
    {
        // Exception sem Properties
        $this->expectException(\InvalidArgumentException::class);
        $simpleProperty = new SimpleProperty('', '', '');
    }

    public function testValidConstructor()
    {
        $simpleProperty = $this->createObject();

        $this->assertEquals("teste", $simpleProperty->getName());
        $this->assertEquals(PropertyType::LONG, $simpleProperty->getType());

        $simpleProperty->setType(PropertyType::BOOL);
        $this->assertEquals(PropertyType::BOOL, $simpleProperty->getType());
    }

    public function testData()
    {
        $simpleProperty = $this->createObject();

        $data = [
            'teste' => [
                'type' => PropertyType::LONG,
            ]
        ];

        $this->assertEquals($data, $simpleProperty->getData());
    }
}
