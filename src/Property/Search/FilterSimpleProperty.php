<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 07/04/17
 * Time: 15:21
 */

namespace CelulaLib\Elastic\Property\Search;

use CelulaLib\Elastic\Property\Create\SimpleProperty;
use CelulaLib\Elastic\Property\PropertyType;

class FilterSimpleProperty extends SimpleProperty implements FilterInterface
{
    use TraitSearchableProperty; // Implements getParameterName, setParameterName from Searchable

    /**
     * @param string $name
     * @param $type
     * @param string $parameterName Default to $name
     */
    public function __construct(string $name, $type, $parameterName = '')
    {
        parent::__construct($name, $type);

        $this->setParameterName($parameterName);
    }

    /**
     * @param SearchParam[]|SearchParam $param
     * @param string $parentName
     * @return array
     */
    public function getFilter($param, string $parentName): array
    {
        if (is_string($param)) {
            $param = new SearchParam($param);
        }
        $valor = $param->getValor();

        // Limpa array com impurezas...
        if (is_array($valor)) {
            $valResult = array();
            foreach ($valor as $value) {
                if ($value !== "") {
                    $valResult[] = $value;
                }
            }
            $valor = $valResult;
        }

        $begin = $valor;
        $end = "";
        // temos inicio e fim ?
        if (is_array($valor) && (count($valor) == 2)
            && ($param->getSearchType() != SearchType::IGUAL_A)) {
            $tmp = array_values($valor);
            $begin = $tmp[0];
            $end = $tmp[1];
        }

        if ($param->getSearchType() == SearchType::IGUAL_A) {
            if (is_array($begin)) {
                return [
                    'terms' => [
                        $this->getFullName($parentName) => $begin
                    ]
                ];
            } else {
                return [
                    'term' => [
                        $this->getFullName($parentName) => $begin
                    ]
                ];
            }
        }

        // Conversao de valores para float em caso de float
        if ($this->getType() == PropertyType::FLOAT) {
            $begin = (float) str_replace(',', '.', $begin);
            $end = (float) str_replace(',', '.', $end);
        }

        switch ($this->getType()) {
            case PropertyType::STRING:
                switch ($param->getSearchType()) {
                    case SearchType::CONTEM:

                }
            case PropertyType::FLOAT:
            case PropertyType::LONG:
                switch ($param->getSearchType()) {
                    case SearchType::MAIOR_QUE:
                        return array(
                            'range' => array(
                                $this->getFullName($parentName) => array (
                                    'gte' => $begin
                                )
                            )
                        );
                    case SearchType::MENOR_QUE:
                        return array(
                            'range' => array(
                                $this->getFullName($parentName) => array (
                                    'lte' => $begin
                                )
                            )
                        );
                    case SearchType::NO_INTERVALO:
                        return array(
                            'range' => array(
                                $this->getFullName($parentName) => array (
                                    'gte' => $begin,
                                    'lte' => $end
                                )
                            )
                        );
                    case SearchType::MULTIPLOS_VALORES:
                        return array(
                            'terms' => array(
                                $this->getFullName($parentName) => $valor
                            )
                        );
                }
                break; // LONG
            case PropertyType::KEYWORD:
                switch ($param->getSearchType()) {
                    case SearchType::MULTIPLOS_VALORES:
                        $multiplos = array();
                        foreach ($valor as $item) {
                            $multiplos[] = strtoupper($item);
                        }

                        return array(
                            'terms' => array(
                                $this->getFullName($parentName) => $multiplos
                            )
                        );
                }
                break; // KEYWORD
        }

        return array();
    }
}
