<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 03/04/17
 * Time: 16:55
 */

namespace CelulaLib\Elastic\Property\Search;

use CelulaLib\Elastic\Property\Create\DateProperty;

class FilterDateProperty extends DateProperty implements FilterInterface
{
    use TraitSearchableProperty; // Implements getParameterName, setParameterName from Searchable

    /**
     * SimpleProperty constructor.
     * @param string $name
     * @param string $parameterName
     */
    public function __construct(string $name, $parameterName = '')
    {
        parent::__construct($name);

        $this->setParameterName($parameterName);
    }

    /**
     * @param SearchParam[]|SearchParam $param
     * @param string $parentName
     * @return array
     */
    public function getFilter($param, string $parentName): array
    {
        $valor = $param->getValor();
        $searchType = $param->getSearchType();

        $begin = $valor;
        $end = $valor;
        if (is_array($valor)) {
            $tmp = array_values($valor);
            $begin = $this->fixDate($tmp[0]);

            if ((count($valor) == 2) && (!empty($tmp[1]))) {
                $end = $this->fixDate($tmp[1]);
            } else {
                $end = $this->fixDate($tmp[0]);
            }
        }

        if ($begin instanceof \DateTime) {
            $begin = $begin->format("Y-m-d");
        }

        if ($end instanceof \DateTime) {
            $end = $end->format("Y-m-d");
        }

        switch ($searchType) {
            case SearchType::IGUAL_A:
                return array(
                    'range' => array(
                        $this->getFullName($parentName) => array(
                            'gte' => $begin,
                            'lte' => $end,
                            'format' => 'yyyy-MM-dd'
                        )
                    )
                );
                break;
            case SearchType::MAIOR_QUE:
                return array(
                    'range' => array(
                        $this->getFullName($parentName) => array(
                            'gte' => $begin,
                            'format' => 'yyyy-MM-dd'
                        )
                    )
                );
                break;
            case SearchType::MENOR_QUE:
                return array(
                    'range' => array(
                        $this->getFullName($parentName) => array(
                            'lte' => $begin,
                            'format' => 'yyyy-MM-dd'
                        )
                    )
                );
                break;

        }
    }
}