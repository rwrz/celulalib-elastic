<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 04/04/17
 * Time: 15:17
 */

namespace CelulaLibTest\Elastic\Property;

use CelulaLib\Elastic\ElasticSearchIndex;
use CelulaLib\Elastic\Mapping;
use CelulaLib\Elastic\Property\Create\DateProperty;
use CelulaLib\Elastic\Property\PropertyCollection;
use CelulaLib\Elastic\Property\PropertyType;
use CelulaLib\Elastic\Property\Search\FilterDateProperty;
use CelulaLib\Elastic\Property\Search\FilterDateRangeProperty;
use CelulaLib\Elastic\Property\Search\FilterSimpleProperty;
use CelulaLib\Elastic\Property\Search\SearchParam;
use CelulaLib\Elastic\Property\Search\SearchType;
use CelulaLib\Elastic\Property\Search\TextProperty;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Class ElasticSearchIndexTest
 * @package CelulaLibTest\Elastic\Property
 */
class ElasticSearchIndexTest extends TestCase
{
    /**
     * @var Client
     */
    protected $elasticSearchClient;
    protected $mappings;
    protected $properties;
    protected $attributes;
    protected $data;

    private function createElasticClient()
    {
        $hosts = array("localhost:9200");
        return ClientBuilder::create()->setHosts($hosts)->build();
    }

    public function setUp()
    {
        // Mock do ElasticSearchClient
        //$this->elasticSearchClient = $this->getMockBuilder(Client::class)->disableOriginalConstructor()->getMock();
        $this->elasticSearchClient = $this->createElasticClient();

        $propertyCollection = new PropertyCollection('tipo_documento', false);
        $propertyCollection->addProperty(
            new FilterSimpleProperty(
                'codigo',
                PropertyType::INTEGER,
                'tipo_documento_codigo'
            )
        );
        $propertyCollection->addProperty(
            new TextProperty(
                'descricao',
                'tipo_documento_descricao'
            )
        );

        $propertyNestedCollection = new PropertyCollection('criterios', true);
        $propertyNestedCollection->addProperty(
            new FilterSimpleProperty(
                'codigo',
                PropertyType::INTEGER,
                'criterios_codigo'
            )
        );
        $propertyNestedCollection->addProperty(
            new TextProperty(
                'descricao',
                'criterios_descricao'
            )
        );
        $propertyNestedCollection->addProperty(
            new FilterDateProperty(
                'data',
                'criterios_data'
            )
        );
        $propertyNestedCollection->addProperty(
            new FilterDateRangeProperty(
                'intervalo_data',
                'criterios_intervalo_data'
            )
        );

        $this->properties = [
            new FilterSimpleProperty('codigo', PropertyType::INTEGER),
            new TextProperty('nome'),
            new DateProperty('data'),
            $propertyCollection,
            $propertyNestedCollection
        ];

        $this->attributes = [
            "numeric_detection" => true,
        ];

        $this->mappings = [
            new Mapping('documento', $this->properties, $this->attributes)
        ];

        $data = new \DateTime();
        $dataFim = new \DateTime();
        $this->data = [
            'codigo' => 1,
            'nome' => 'Rodrigo',
            'data' => $data,
            'tipo_documento' => [
                'codigo' => 1,
                'descricao' => 'tipo documento 1',
                'data' => $data
            ],
            'criterios' => [
                [
                    'codigo' => 1,
                    'data' => $data,
                    'descricao' => 'criterio 1',
                    'intervalo_data' => array (
                        $data,
                        $dataFim->add(new \DateInterval("P2D"))
                    )
                ],
                [
                    'codigo' => 2,
                    'data' => $data,
                    'descricao' => 'criterio 2',
                    'intervalo_data' => array (
                        $data,
                        $dataFim->add(new \DateInterval("P10D"))
                    )
                ]
            ]
        ];
    }

    public function testInvalidConstructor()
    {
        $this->expectException(\InvalidArgumentException::class);
        $elasticSearchIndex = new ElasticSearchIndex('', array(), $this->elasticSearchClient);
    }

    public function testValidConstructorAndBasicMethods()
    {
        $name = 'ged-teste';
        $elasticSearchIndex = new ElasticSearchIndex($name, $this->mappings, $this->elasticSearchClient);
        $this->assertEquals($this->mappings, $elasticSearchIndex->getMappings());
        $this->assertEquals($name, $elasticSearchIndex->getName());
        $this->assertEquals($this->elasticSearchClient, $elasticSearchIndex->getElasticSearchClient());

        $elasticSearchIndex->setName('aloha');
        $this->assertEquals('aloha', $elasticSearchIndex->getName());

        $tmpMappings = new Mapping('documento2', $this->properties, $this->attributes);
        $elasticSearchIndex->setMappings($tmpMappings);
        $this->assertEquals($tmpMappings, $elasticSearchIndex->getMappings());

        $elasticSearchIndex->setSettings(array());
        $this->assertEquals(array(), $elasticSearchIndex->getSettings());

        return $elasticSearchIndex;
    }

    public function testCreateIndex()
    {
        // Criando o mesmo indice do ultimo test, tem q gerar erro
        $name = 'ged-teste';
        $elasticSearchIndex = new ElasticSearchIndex($name, $this->mappings, $this->elasticSearchClient);

        // Deleta para podermos criar
        $elasticSearchIndex->delete(); // limpa primeiro

        // Cria
        $success = [
            'acknowledged' => true,
            'shards_acknowledged' => true
        ];
        $this->assertEquals($success, $elasticSearchIndex->create());

        // Force creation again (delete + criar)
        $this->assertEquals($success, $elasticSearchIndex->create(true));

        // ERRO pois o indice ja existe
        $this->expectException(\Exception::class); //-- POR ULTIMO, pois depois da exception o teste morre
        $elasticSearchIndex->create();
    }

    public function testInsert()
    {
        $name = 'ged-teste';
        $elasticSearchIndex = new ElasticSearchIndex($name, $this->mappings, $this->elasticSearchClient);
        $elasticSearchIndex->create(true);

        // criar
        $result = $elasticSearchIndex->insert('documento', $this->data['codigo'], $this->data);
        $this->assertTrue($result['created']);

        // ja criado ?
        $result = $elasticSearchIndex->insert('documento', $this->data['codigo'], $this->data);
        $this->assertFalse($result['created']);

        // Mapping nao existente
        $this->expectException(\Exception::class);
        $result = $elasticSearchIndex->insert('documento2', $this->data['codigo'], $this->data);
    }

    public function testQueryMappingError()
    {
        $name = 'ged-teste';
        $elasticSearchIndex = new ElasticSearchIndex($name, $this->mappings, $this->elasticSearchClient);

        // ja deve estar criado...
        $this->expectException(\Exception::class);
        $result = $elasticSearchIndex->getQuery('documentoasdasd', array());
    }

    public function testQueryMapp()
    {
        $name = 'ged-teste';
        $elasticSearchIndex = new ElasticSearchIndex($name, $this->mappings, $this->elasticSearchClient);
        $elasticSearchIndex->create(true);
        $data = new \DateTime();

        // criar
        $result = $elasticSearchIndex->insert('documento', $this->data['codigo'], $this->data);
        $this->assertTrue($result['created']);

        $result = $elasticSearchIndex->getQuery(
            'documento',
            array(
                'tipo_documento_descricao' => new SearchParam('1', SearchType::CONTEM_PALAVRA),
                'nome' => new SearchParam('rodrigo', SearchType::CONTEM_PALAVRA),
                'criterios_descricao' => new SearchParam('1', SearchType::CONTEM_PALAVRA),
                'criterios_data' => new SearchParam($data->format('Y-m-d'))
            )
        );

        $expectResult = array(
            'index' => $name,
            'type'  => 'documento',
            'size'  => 5,
            'scroll' => '300s',
            'body'  => array(
                'query' => array(
                    'bool' => array(
                        'must'  => array (
                            array (
                                'match' => array (
                                    'nome_string' => 'RODRIGO'
                                )
                            ),
                            array (
                                'match' => array (
                                    'tipo_documento.descricao_string' => '1'
                                )
                            ),
                            array (
                                'nested' => array (
                                    'path' => 'criterios',
                                    'inner_hits' => array (
                                        'name' => '1',
                                        'highlight' => array (
                                            'pre_tags' => array ("<span class='w-700 text-danger'>"),
                                            'post_tags' => array ("</span>"),
                                            'fields' => array (
                                                array (
                                                    'criterios.descricao_string' => new \stdClass()
                                                ),
                                                array (
                                                    'criterios.descricao_keyword' => new \stdClass()
                                                ),
                                                array (
                                                    'criterios.data' => new \stdClass()
                                                )
                                            )
                                        )
                                    ),
                                    'query' => array (
                                        'bool' => array (
                                            'must' => array (
                                                array (
                                                    'match' => array (
                                                        'criterios.descricao_string' => '1',
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            ),
                            array (
                                'nested' => array (
                                    'path' => 'criterios',
                                    'inner_hits' => array (
                                        'name' => '2',
                                        'highlight' => array (
                                            'pre_tags' => array ("<span class='w-700 text-danger'>"),
                                            'post_tags' => array ("</span>"),
                                            'fields' => array (
                                                array (
                                                    'criterios.descricao_string' => new \stdClass()
                                                ),
                                                array (
                                                    'criterios.descricao_keyword' => new \stdClass()
                                                ),
                                                array (
                                                    'criterios.data' => new \stdClass()
                                                )
                                            )
                                        )
                                    ),
                                    'query' => array (
                                        'bool' => array (
                                            'filter' => array (
                                                'bool' => array (
                                                    'must' => array (
                                                        array (
                                                            'range' => array (
                                                                'criterios.data' => array (
                                                                    'gte'    => $data->format("Y-m-d"),
                                                                    'lte'    => $data->format("Y-m-d"),
                                                                    'format' => 'yyyy-MM-dd'
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                ),
                "highlight" => array (
                    "fields" => array (
                        array (
                            "nome_string" => new \stdClass()
                        ),
                        array (
                            "nome_keyword" => new \stdClass()
                        ),
                        array (
                            "tipo_documento.descricao_string" => new \stdClass()
                        ),
                        array (
                            "tipo_documento.descricao_keyword" => new \stdClass()
                        )
                    )
                ),
                'sort'  => array(
                    '_score' => array(
                        'order' => 'desc'
                    )
                )
            )
        );

        $this->assertEquals($expectResult, $result);
    }

    public function testSearch()
    {
        sleep(2); // triste, mas necessario
        $name = 'ged-teste';
        $elasticSearchIndex = new ElasticSearchIndex($name, $this->mappings, $this->elasticSearchClient);
        $data = new \DateTime();
        $intervaloData = new \DateTime();
        $intervaloData->add(new \DateInterval("P1D"));

        $result = $elasticSearchIndex->search(
            'documento',
            array(
                'tipo_documento_descricao' => new SearchParam('1', SearchType::CONTEM_PALAVRA),
                'nome' => new SearchParam('rodrigo', SearchType::CONTEM_PALAVRA),
                'criterios_descricao' => new SearchParam('1', SearchType::CONTEM_PALAVRA),
                'criterios_data' => new SearchParam(
                    array (
                        'inicio' => $data->format("Y-m-d"),
                        'fim' => $data->add(new \DateInterval("P2D"))->format("Y-m-d")
                    )
                ),
                'criterios_intervalo_data' => new SearchParam(
                    array (
                        'inicio' => $intervaloData->format("Y-m-d"),
                        'fim' => $intervaloData->add(new \DateInterval("P2D"))->format("Y-m-d")
                    )
                )
            )
        );
        $this->assertEquals(1, $result['hits']['total']);
    }
}
