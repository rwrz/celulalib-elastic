<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 30/03/17
 * Time: 10:50
 */

namespace CelulaLib\Elastic\Property;

interface PropertyInterface
{
    public function getName() : string;
    public function getData() : array;
    public function getValue($value) : array;
    public function getHighlight(string $parentName) : array;
    public function getFullName(string $parentName);
}