<?php

namespace CelulaLib\Elastic;

use Application\Enum\TipoCriterio;
use Elasticsearch\Client;
use GEDDigital\Entity\Documento as DocumentoEntity;
use Zend\Db\ResultSet\ResultSet;

abstract class AbstractCelulaElastic implements ICelulaElastic
{
    /**
     * @var Client
     */
    protected $eClient;

    /**
     * @var DocumentoEntity[]|ResultSet
     */
    protected $documentos;

    /**
     * @var array
     */
    protected $params = array();

    public function execute()
    {
        if($this instanceof Inserter) {
            $this->eClient->index($this->params);
        } else if($this instanceof Updater) {
            $this->eClient->update($this->params);
        }
        $this->params = array();
    }

    /**
     * Processa os Criterios
     * @param DocumentoEntity $documento
     */
    protected function processCriterios(DocumentoEntity $documento)
    {
        foreach ($documento->getCriterios() as $criterio) {
            switch ($criterio->getTipoPastaCriterio()->getTipo()) {
                case TipoCriterio::DATA:
                case TipoCriterio::INTERVALO_DE_DATAS:
                    $field = 'valor_date';
                    $date = \DateTime::createFromFormat('d/m/Y', $criterio->getValor());
                    $value = !$date ? $criterio->getValor() : $date->format('Y-m-d');
                    break;
                case TipoCriterio::CONDICIONAL:
                    $field = 'valor_boolean';
                    $value = boolval($criterio->getValor());
                    break;
                case TipoCriterio::NUMEROS_INTEIROS:
                    $field = 'valor_long';
                    $value = (int) $criterio->getValor();
                    break;
                case TipoCriterio::NUMEROS_INTEIROS_E_FLUTUANTE:
                case TipoCriterio::INTERVALO_DE_NUMEROS:
                    $field = 'valor_float';
                    $value = (float) str_replace(',', '.', $criterio->getValor());
                    break;
                default:
                    $field = 'valor_string';
                    $value = $criterio->getValor();
                    break;
            }

            $structure = array(
                $field => $value,
                'valor_keyword' => is_bool($value) || is_numeric($value) ? $value : strtoupper($value),
                "tipo_pasta_criterio" => array(
                    "codigo" => $criterio->getTipoPastaCriterio()->getCodigo(),
                    "descricao" => $criterio->getTipoPastaCriterio()->getDescricao()
                )
            );

            if($this instanceof Inserter) {
                $this->params['body']['criterios'][] = $structure;
            } elseif ($this instanceof Updater) {
                $this->params['body']['doc']['criterios'][] = $structure;
            }
        }
    }
}