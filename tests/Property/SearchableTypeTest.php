<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 04/04/17
 * Time: 11:08
 */

namespace CelulaLibTest\Elastic\Property;

use CelulaLib\Elastic\Property\Search\SearchableType;
use PHPUnit\Framework\TestCase;

class SearchableTypeTest extends TestCase
{
    public function testConstructor()
    {
        $this->expectException(\Exception::class);
        $searchableType = new SearchableType();
    }
}